Auteurs: Hawa Sogoba, Adiiya Gilmanova.

Question: Est-ce qu'il y a un lien significative entre empreinte carbone et les annees en bonne sante au niveau mondial?

Status : On a choisi les donnees de The World bank pour les emissions de carbon - https://databank.worldbank.org/reports.aspx?source=2&series=EN.ATM.CO2E.PC&country=#

	et les donnees de GHO Organisation mondiale de la Sante pour l'esperance de vie en bonne sante - http://apps.who.int/gho/data/view.main.HALEXv?lang=fr